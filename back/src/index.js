import app from './app'
import initializeDatabase from './db'

const start = async () => {
  const controller = await initializeDatabase()
  app.get('/', (req, res) => res.send("ok"));

  app.get("/articles/list", async (req, res) => {
      const { order, desc } = req.query;
      const articles = await controller.getArticlesList({order});
      res.json({ success: true, result: articles });
    });
  
    app.get("/externalLinks/list", async (req, res) => {
      const { order, desc } = req.query;
      const externalLinks = await controller.getExternalLinks({order});
      res.json({ success: true, result: externalLinks });
    });
  
  app.get('/articles/getarticle/:id', async (req,res)=> {
    const {id} = req.params
    const article = await controller.getArticle(id)
    res.json({success:true, result:article})
  })
  app.listen(4444, () => console.log('server listening on port 4444'))
}
start();

