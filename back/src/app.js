import express from 'express'
import cookieParser from 'cookie-parser' 
import session from 'express-session' 
import favicon from 'serve-favicon' 
import cors from 'cors' 
import createError from 'http-errors' 
import path from 'path'

const app = express(); 

const IS_PRODUCTION = app.get('env') === 'production'

if (IS_PRODUCTION) {
  app.set('trust proxy', 1) 
}

app.use(cors()) 
app.use(express.json()); 
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser()); 
app.use(favicon(path.join(__dirname, '../public', 'favicon.ico'))) 
app.use(express.static(path.join(__dirname, '../public')));

app.use(session({ 
  secret: 'fine', 
  cookie: { secure: IS_PRODUCTION }, 
  resave: true,
  saveUninitialized: true
}))


export default app
