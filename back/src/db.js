import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');
  
  const getArticlesList = async props => {
    const { order, desc } = props;
   
          let statement = `SELECT  id, title, text, date, img_path , link FROM articles`
        switch(order){
          case 'date': statement+= ` ORDER BY date DESC`; break;
          default: break;} 
  
      const rows = await db.all(statement);
      return rows;}
      
  const getArticle = async (id) => {

    const articlesList = await db.all(SQL `select id,title,text,img_path, link FROM articles where id = ${id}` )
    const article = articlesList[0]
    if(article) {
      return article
    }
    return(article)
  }
  const getExternalLinks = async props => {
    const { order, desc } = props;
   
          let statement = `SELECT  id, title, text, date, img_path , link FROM articles WHERE link != ''`
        switch(order){
          case 'date': statement+= ` ORDER BY date DESC`; break;
          default: break;} 
  
      const rows = await db.all(statement);
      return rows;}
      
  
const controller = {
    getArticlesList,
    getArticle,
    getExternalLinks
  }

  return controller
}

export default initializeDatabase