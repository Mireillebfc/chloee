
import React from 'react'
import LatestArticles from './latestArticles'
import ExternalLinks from './externalLinks'
import Overview from './overview'

export default class Home extends React.Component {
  render(){
    
    return(
      <React.Fragment>  
      <Overview/>  
      <LatestArticles articles={this.props.articles}/>
      <ExternalLinks externalLinks={this.props.externalLinks} /> 
      </React.Fragment>
    )
  }
}
