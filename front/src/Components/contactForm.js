import React from "react"

export default class ContactForm extends React.Component {
    state  = {
            firstName: "",
            lastName: "",
            email:"",
            phoneNo: "",
            subject: "",
            message:""
    }
  
    handleChange =(event)=> {
        const {name, value} = event.target
        this.setState ({[name]: value})
    }

   handleSubmit=(event)=>{
    event.preventDefault();

        const {firstName, lastName, email, phoneNo, subject, message}= this.state
        alert (firstName+"\n"+lastName+"\n"+email+"\n"+phoneNo+"\n"+subject+"\n"+message);
        
    }
    
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>First Name:</label>
                <br/>
                <input 
                    type="text" 
                    value={this.state.firstName} 
                    name="firstName" 
                    
                    onChange={this.handleChange} 
                />
                <br />
                <label>Last Name:</label>
                <br/>
                 <input 
                    type="text" 
                    value={this.state.lastName} 
                    name="lastName" 
                 
                    onChange={this.handleChange} 
                />
            <br />
            <label>Email:</label>
                <br/>
                <input 
                    type="text" 
                    value={this.state.email} 
                    name="email" 
                    
                    onChange={this.handleChange} 
                />
            <br />
            <label>Phone Number:</label>
                <br/>
                <input 
                    type="text" 
                    value={this.state.phoneNo} 
                    name="phoneNo" 
                    
                    onChange={this.handleChange} 
                />
                <br />
                <label>Subject:</label>
                <br/>
                <input 
                    type="text" 
                    value={this.state.subject} 
                    name="subject" 
                    
                    onChange={this.handleChange} 
                />
                <br />
                <label>Message:</label>
                <br/>
                <textarea 
                    value={this.state.value}
                    name="message"
                    onChange={this.handleChange}
                    />
                    <br />                                 
                <button>Submit</button>
            </form>
        )
    }
}