import React from 'react'
import LatestArticle from './latestArticle'

export default class LatestArticles extends React.Component{
 

    render(){     
      const {articles}=this.props;
    
    return(
      <React.Fragment>
      <h2>Latest Articles</h2>
      <section className="card-list">

  {articles.filter((i ,index) => (index < 3)).map((i,index)=> (<LatestArticle  key={i.id} article={i}></LatestArticle>))
  }
      </section>
      </React.Fragment>
      
    )
  }
}