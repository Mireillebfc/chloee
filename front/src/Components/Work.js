import React from "react";
import ArticlesList from './articlesList'

export default class Work extends React.Component {
  render() {
    return (
         <ArticlesList  articles={this.props.articles}/>
    );
  }
}