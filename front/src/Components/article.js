import React from 'react'

export default class Article extends React.Component {
  render(){
    return(
      <React.Fragment>
      <article className="main-content">
    <section>
      <h2>{this.props.article.title}</h2>
      <p>{this.props.article.text}</p>
      <img src="images/ball.png" alt="" className="align-right" />
      </section>
    </article>
    </React.Fragment> )}}