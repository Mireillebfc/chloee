
import React from 'react'
import { Link } from "react-router-dom";
export default class LatestArticle extends React.Component{

render(){
  const {article} =this.props;
return(
  
  <React.Fragment>
  <div className="card">
       <img src="images/placeholder1.png" alt=""/>
       <div class="card-details">
       <h3>{article.title}</h3>
       <p>{article.date}</p>
       <p>{article.text}</p>
       <p><Link to={`/articles/${article.id}`}>Read more</Link></p>
       </div>
      </div>
</React.Fragment>)}}