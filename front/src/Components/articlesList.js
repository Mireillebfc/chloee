import React from 'react'
import Article from './article'

export default class ArticlesList extends React.Component {
 
  render(){
    return(
    <ul>
{this.props.articles.map(article=>(<Article key={article.id} article={article} />))}
</ul>
    )
  }
}