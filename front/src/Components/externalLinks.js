import React from 'react'
import ExternalLink from './externalLink'

export default class ExternalLinks extends React.Component 
{
  render()
  {return (
      <aside>
        <h3>External Links</h3>
        <ul>
          {this.props.externalLinks.map(externalLink => (<ExternalLink key={externalLink.id} externalLink={externalLink} />))}
</ul>
     {/*} <ul>
        <li><a href="">Title 1</a>
          <p>some text</p>
        </li>
        <li><a href="">Title 2</a>
          <p>some text</p>
        </li>
        <li><a href="">Title 3</a>
          <p>some text </p>
        </li>
        <li><a href="">Title 4</a>
          <p>some text</p>
        </li>
      </ul>*/}
       { /*{
        this.props.articles.filter((i, index) => (index < 5)).map((i, index) => 
        { 
          return (this.props.article.link === !null ? 
          <ExternalLink key={i.id} title={i.title} link={i.link} /> : 
          <p></p>) 
        })
      }*/}
    </aside>
    )
  }
}