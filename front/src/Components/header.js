import React from "react";
import { Link } from "react-router-dom";


export default class Navbar extends React.Component {
  render() {
    return (
    <header className="header">
    <img src="images/logo.png" alt="Chloe Domat"/>
   <nav><ul className="nav">
            <li><Link to="/" className="active">Home</Link></li>
            <li><Link to="/Work">Work</Link></li>
            <li><Link to="/Biography">Biography</Link></li>
            <li><Link to="/ContactMe">ContactMe</Link></li>
            </ul></nav></header>
    );
  }
}
