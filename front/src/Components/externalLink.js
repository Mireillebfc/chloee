import React from 'react'

export default class ExternalLink extends React.Component{
  render(){
  return (
    <div>
      <ul>
        <li><p>{this.props.externalLinks.title}</p>
          <p>{this.props.externalLinks.link} </p>
        </li>
        </ul>
    </div>
  );
}
}
