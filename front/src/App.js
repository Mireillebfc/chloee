import React, { Component } from 'react'
import {Switch, Route} from 'react-router-dom'
import Header from './Components/header'
import Footer from './Components/footer'
import Home from './Components/Home'
import Biography from './Components/Biography'
import ContactMe from './Components/ContactMe'
import Work from './Components/Work'
import Quote from './Components/quote'
import './App.css';


class App extends Component {
  
  state={
    articles_list:[],
    externalLinks:[]

  }
 getList = async()=>{
    const response = await fetch(`http://localhost:4444/articles/list?order=date`)
    const data = await response.json()
    if(data){
      const articles_list = data.result
      this.setState({articles_list})
    }  
}
getExternalLinks = async()=>{
  const response = await fetch(`http://localhost:4444/externalLinks/list?order=date`)
  const data = await response.json()

  console.log("externalLinks", data);
  if(data){
    const externalLinks = data.result
    this.setState({externalLinks})
  }  
}
  componentDidMount(){
    this.getList();
    this.getExternalLinks();
  }

  render() {
    
      return (
        <React.Fragment>
        <Header />
        <Quote />
        <Switch>
          <Route exact path="/" render={() => <Home articles={this.state.articles_list}/>} externalLinks={this.state.externalLinks}/>
          <Route path="/Work" render={() => <Work articles={this.state.articles_list}/>}/>
          <Route path="/Biography" component={Biography} />
          <Route path="/ContactMe" component={ContactMe} />
        </Switch>
        <Footer /> 
      </React.Fragment>
    );
  }
}

export default App;
